import java.util.Scanner;

public class Questions {
    
    public static void main (String[] args) {
        
        
        //Print question
        String q1= "Como te llamas?";
        System.out.println(q1);
        
        //Initialize scanner variable
        Scanner scq1 = new Scanner(System.in);
        
        //Create variable for answer
        String respq1;
        
        //Read the answer given and store it into variable
        respq1 = scq1.nextLine(); 
       
        //Print question
        String q2= "Cual es tu edad?";
        System.out.println(q2);
        
        //Initialize scanner variable
        Scanner scq2 = new Scanner(System.in);
        
        //Create variable for answer
        String respq2;
        
        //Read the answer given and store it into variable
        respq2 = scq2.nextLine(); 
        
       
         //Print question
        String q3= "En que Estado de Mexico vives?";
        System.out.println(q3);
        
        //Initialize scanner variable
        Scanner scq3 = new Scanner(System.in);
        
        //Create variable for answer
        String respq3;
        
        //Read the answer given and store it into variable
        respq3 = scq3.nextLine(); 
       
       
       
       //Print/display answer(s) given
        //System.out.println("Tu respuesta a la pregunta 1 es: " +respq1);
        //System.out.println("Tu respuesta a la pregunta 2 es: " +respq2);
        //System.out.println("Tu respuesta a la pregunta 3 es: " +respq3);
        
        System.out.println("Bienvenido " +respq1+ ", tu edad es " +respq2+ " y vives en el Estado de "+respq3);
    }
        
}